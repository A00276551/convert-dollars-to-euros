
public class convertDollarsToEuros {
	
	public static double convertDollarToEuro(double dollar, double conversionRate) {
	
	// return value
	double euro = dollar * conversionRate;
	
	// 0 and negative values will return -99.0
	if (euro <= 0)
		euro = -99.0;
	else
		euro = dollar * conversionRate; //Please check regularly for new conversion rates and update if necessary
	
	return euro;
	
	}
	
}
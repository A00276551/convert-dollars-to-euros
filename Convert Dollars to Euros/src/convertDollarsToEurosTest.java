import junit.framework.TestCase;

public class convertDollarsToEurosTest extends TestCase {
	
	// Test Number: 1
	// Test Objective: To test if a negative number returns -99.0
	// Test input(s): dollar = -50.0, conversionRate = 0.90
	// Test expected output(s): -99.0
	
	public void testConvertDollarToEuro001() {
		
		assertEquals(-99.0, convertDollarsToEuros.convertDollarToEuro(-50.0, 0.90));
	}

	// Test Number: 2
	// Test Objective: To test if 0.0 returns -99.0
	// Test input(s): dollar = 0.0, conversionRate = 0.90
	// Test expected output(s): -99.0
	
	public void testConvertDollarToEuro002() {
		
		assertEquals(-99.0, convertDollarsToEuros.convertDollarToEuro(0.0, 0.90));
	}
	
	// Test Number: 3
	// Test Objective: To test if the return value is calculated correctly to a number correct to 2 decimal places
	// Test input(s): dollar = 15.0, conversionRate = 0.90
	// Test expected output(s): 13.5
	
	public void testConvertDollarToEuro003() {
		
		assertEquals(9.0, convertDollarsToEuros.convertDollarToEuro(10.0, 0.9));
	}

}
